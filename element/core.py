from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from .element import Element
from services.logger import log_find, log_action


class Core:
    def __init__(self, driver):
        self._driver = driver
        self._condition = EC.visibility_of_element_located
        self._by = By.XPATH
        self.element = None
        self.list_element = []

    @log_action
    def click(self):
        """Clicking on element"""
        try:
            self.element.click()
        except AttributeError:
            self.element = None
        return self

    @log_find
    def find(self, locator: Element):
        """Find element. Instance of Element class is accepted only """
        condition, by, locator = locator()
        try:
            self.element = WebDriverWait(self._driver, 10).until(condition((by, locator)))
        except TimeoutException:
            self.element = None
        return self

    def find_list(self, locator: Element):
        """Find list of elements if any"""
        condition, by, locator = locator()
        self.list_element = WebDriverWait(self._driver, 10).until(EC.presence_of_all_elements_located((by, locator)))
        return self

    @log_action
    def get(self, i: int):
        """ i is an element's index in list of elements to be set as element property. Self is returned from function"""
        if isinstance(self.list_element, list):
            try:
                self.element = self.element[i]
            except IndexError:
                raise IndexError(f'Trying to get "{i}" element from the list "{self.list_element}"')
        else:
            raise IndexError(f'Element is not a list')
        return self

    @log_action
    def fill(self, data: str):
        """Fill field with data"""
        self.element.click()
        self.element.clear()
        self.element.send_keys(data)
        return self

    @log_action
    def get_text(self):
        """Get elements text"""
        try:
            return self.element.text
        except AttributeError:
            return None

    @log_action
    def get_value(self, attr="value"):
        """Get attribute value name. VALUE is set by default"""
        try:
            return self.element.get_attribute(str(attr))
        except AttributeError:
            return None
