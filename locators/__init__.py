from .home_page_locators import HomePageLocators, NavBarLocators, LanguageSelector
from .create_voomie_locators import CreateVoomieLocators
from .base_locators import CameraLocators, ErrorLocators, HowToPageLocators
