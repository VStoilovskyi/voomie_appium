from element import Element as E
from appium.webdriver.common.mobileby import MobileBy as By


class CameraLocators(object):
    CAPTURE = E('com.android.camera2:id/shutter_button')
    CLOSE = E('com.android.camera2:id/btn_cancel')
    DONE = E('com.android.camera2:id/btn_done')


class ErrorLocators(object):
    TEXT = E('android:id/message')
    OK = E('android:id/button1')


class HowToPageLocators(object):
    SKIP = E('com.voomie.videolivewallpaper:id/btSkip')
