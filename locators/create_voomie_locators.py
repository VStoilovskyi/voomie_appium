from element import Element as E
from appium.webdriver.common.mobileby import MobileBy as By


class CreateVoomieLocators(object):
    VOOMIE_VIDEOS_LIST = E(locator='com.voomie.videolivewallpaper:id/sqFlContainer')
    VOOMIE_VIDEO_ELEMENT = E(locator='com.voomie.videolivewallpaper:id/ivPreview')
    CAMERA_LOCATOR = E(locator='com.voomie.videolivewallpaper:id/vCamera')





