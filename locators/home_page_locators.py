from element import Element as E
from appium.webdriver.common.mobileby import MobileBy as By
from constants import NavigationTextEng, NavigationTextHindi


class BaseLanguageLocators(object):

    def __init__(self, is_english: bool = True):
        self.lang = NavigationTextEng if is_english else NavigationTextHindi


class HomePageLocators(BaseLanguageLocators):

    def __init__(self, is_english=True):
        super().__init__(is_english)

    CREATE_NEW_VOOMIE_BTN = E('com.voomie.videolivewallpaper:id/btCreate')
    VOOMIE_LIBRARY_BTN = E('com.voomie.videolivewallpaper:id/btLibrarie')

    def nav_icon(self):
        return E(locator=f'//android.widget.ImageButton[@content-desc="{self.lang.open}"]', by=By.XPATH)

    WALLPAPER_SWITCHER = E(locator='com.voomie.videolivewallpaper:id/swActivate')
    LOOP_SWITCHER = E(locator='com.voomie.videolivewallpaper:id/swLoop')

    BACK_BTN = E(locator='Navigate up', by=By.ACCESSIBILITY_ID)


class NavBarLocators(object):
    LIST_ELEMENT = E(locator='/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.'
                             'FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.'
                             'widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.'
                             'widget.LinearLayout', by=By.XPATH)
    FEEDBACK = E('com.voomie.videolivewallpaper:id/llFeedBack')
    FAQ = E('com.voomie.videolivewallpaper:id/llFAQ')
    LANGUAGE = E('com.voomie.videolivewallpaper:id/llLanguage')


class LanguageSelector(object):
    ENGLISH = E(locator='''//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]''', by=By.XPATH)

    HINDI = E(locator='//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]', by=By.XPATH)
    CHECK_ICON = E(locator='com.voomie.videolivewallpaper:id/ivLanguage', by=By.ID)
    LANG_ELEMENT_FROM_ICON = E(locator='/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.'
                                       'widget.FrameLayout/android.widget.LinearLayout/android.widget.'
                                       'FrameLayout/android.view.View/android.widget.FrameLayout/android.widget.'
                                       'RelativeLayout/android.view.View/androidx.recyclerview.widget.'
                                       'RecyclerView/android.view.View[1]/android.widget.ImageView', by=By.XPATH)

    @staticmethod
    def get_lang_text_by_option_id(option_id):
        return E(locator=f'//android.view.ViewGroup[{option_id}]/android.widget.ImageView', by=By.XPATH)

    @staticmethod
    def get_lang_name_by_option_id(option_id):
        return E(locator=f'''//androidx.recyclerview.widget.RecyclerView/android.view.View[{option_id}]/
                             android.widget.ImageView/../android.widget.TextView''', by=By.XPATH)
