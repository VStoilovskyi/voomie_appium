import logging

logging.basicConfig(format='%(asctime)s | %(levelname)s | %(name)s %(message)s')
log = logging.getLogger()
log.setLevel('INFO')
