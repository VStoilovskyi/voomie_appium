from .log_config import log


def log_find(func):
    def wrapper(self, *args, **kwargs):
        condition, by, locator = args[0]()
        log.info('==== Searching for element ====')
        log.info(f'Element locator: {locator}, By: {by}')
        log.info(f'To be: {condition.__name__}')
        result = func(*args, **kwargs)
        if self.element:
            log.info('Element has been found')
        return result
    return wrapper


def log_action(func):
    def wrapper(*args, **kwargs):
        log.info(f'Trying to {func.__name__} ')
        return func(*args, **kwargs)
    return wrapper
