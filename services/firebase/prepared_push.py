def _prepare_push_dict(text, title, link=None):
    return {
        "text": text,
        "title": title,
        "link": link
    }


class PreparedPush:

    TO_BE_OPEN_IN_BROWSER = _prepare_push_dict('To be opened in Browser', 'Voomie', 'https://google.com')
    TO_BE_DOWNLOADED_IN_APP = _prepare_push_dict('To be downloaded in app', 'Voomie', 'Should be changed!')
