import requests

headers = {
  'authority': 'gcmcontextualcampaign-pa.clients6.google.com',
  'authorization': 'SAPISIDHASH 1588881761_7948871f500cda4a46d97d653e4563f40e720ee8',
  'x-goog-authuser': '1',
  'content-type': 'application/json',
  'origin': 'https://console.firebase.google.com',
  'x-client-data': 'CIe2yQEIo7bJAQipncoBCNCvygEIvLDKAQjttcoBCI66ygEYu7rKARibvsoB',
}

cookie = {
    'ANID':'AHWqTUnHlvH0DgtcJSHl3MrW4AlbKNRURc0IoUJ6aN17ZRre7nj5mVomph3-wuFz',
    'CONSENT':'YES+UA.ru+201906',
    'HSID': 'AiroMbNe3P3sl3xbv',
    'SSID':'ALZhfcFyBu00dyC0S',
    'APISID':'IytcRDuA9iLVXCGE/AC6ItmYTqPQJQIrkI',
    'SAPISID':'pIagm80xg7KA2tIc/Avb8TxQmvBTD2rNdb',
    '__Secure-HSID':'AiroMbNe3P3sl3xbv',
    'Secure-SSID':'ALZhfcFyBu00dyC0S',
    '__Secure-APISID':'IytcRDuA9iLVXCGE/AC6ItmYTqPQJQIrkI',
    'Secure-3PAPISID': 'pIagm80xg7KA2tIc/Avb8TxQmvBTD2rNdb',
    'SEARCH_SAMESITE': 'CgQI1I8B',
    'SID': 'wwc9EUCiEbO-kCTTDVn9RWNxdGYIynHFUa4BUWFANa6QqrceeV8Q2oaMAe1YUvuhZqB8Hw.',
    'Secure-3PSID': 'wwc9EUCiEbO-kCTTDVn9RWNxdGYIynHFUa4BUWFANa6QqrcesAHQw675a4EGkiKt9paQxQ.',
    'NID': 'gUEfpJczGE0BHIIxCo1ePzCo4OfwaSAmvtzMXB3i7ak3js3wjr3Oy-erVdeIiTUafQO1265L2jpagWViNwzHI79RyfT2kudUUnLNGtBd8f9biB2TFaItyWkX6O0yhFsz3DCm4Hf3fl4irRlDFkSLU0i2qcfxp477iHEoO6VdcmyINHsp8ulXRQ0mZD85H3ff3ZHer5-bw2WIIZjDVP78psuaaW03Y3_bOKZP5Kl1x0Kp46q54xcgAO5DtXXpUz4-XEW0ewc7ixJWwFneX5ikI6uCzPL3y09A1itA9L5kXGyohRNkekSu7RQmtDy1FCp1KO0Yz9CxyO2C',
    '1P_JAR': '2020-05-07-20',
    'SIDCC': 'AJi4QfFZDDJaOorvmQUKBTGRaEWqIThXKUXgEGQ5wdZ5K0Uwb4oLbKClUlbKxk87kmc8rxRybIY'

}

url = "https://gcmcontextualcampaign-pa.clients6.google.com/v2/projects/257814059226/campaigns?alt=json&key=AIzaSyDovLKo3djdRbs963vqKdbj-geRWyzMTrg"


def set_push_body(text, title, link):
    model = {
        "basics": {
            "name": ""
        },
        "productAction": {
            "notificationAction": {
                "campaignStatus": "STATUS_ENQUEUED",
                "displayParameters": {
                    "data": [{
                        "key": "url",
                        "value": f"{link}"
                    }] if link else [],
                    "image": "",
                    "priority": "HIGH",
                    "title": f"{title}"
                },
                "expiryTime": "2419200s",
                "messageText": f"{text}"
            }
        },
        "targeting": {
            "or": {
                "conditions": [
                    {
                        "and": {
                            "conditions": [
                                {
                                    "appId": {
                                        "targetGmpAppId": "1:257814059226:android:d5fd6896b8d4598628ede3"
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    }

    return model


def send_push(text, title, link=None):
    return requests.post(url, headers=headers, json=set_push_body(text, title, link), cookies=cookie)


