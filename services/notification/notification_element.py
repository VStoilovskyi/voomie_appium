import time
from element import Core, Element
from selenium.webdriver.common.by import By

BASE_NOTIFICATION_LOCATOR = Element(locator='/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout[1]/'
                                            'android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/'
                                            'android.widget.FrameLayout', by=By.XPATH)


class NotificationElement(object):
    def __init__(self, driver):
        self._driver = driver
        self.element = None

    def find_by_title(self, title):
        element = Core(self._driver)
        try:
            for i in range(10):
                element.find_list(BASE_NOTIFICATION_LOCATOR)

                if not element.list_element:
                    time.sleep(0.25)
                    continue
                for elem in element.list_element:
                    self.element = elem if elem.find_element_by_id('android:id/title').text == title else None
                    break
                time.sleep(0.25)
        finally:
            pass
        return self
