from .notification_element import NotificationElement


class Notification(object):
    def __init__(self, driver):
        self.__driver = driver

    def open_notifications(self):
        self.__driver.open_notifications()

    def find_notification_by_title_and_click(self, title):
        NotificationElement(self.__driver).find_by_title(title).click()
