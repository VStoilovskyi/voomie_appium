from element import Core, Element
from selenium.webdriver.common.by import By

OPEN_WITH_LIST_LOCATOR = Element('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/'
                                 'android.widget.FrameLayout/com.android.internal.widget.ResolverDrawerLayout/'
                                 'android.widget.ListView/android.widget.LinearLayout', by=By.XPATH)


class OpenWith:
    def __init__(self, driver):
        self._driver = driver

    def __find_open_with_list(self):
        return Core(self._driver).find_list(OPEN_WITH_LIST_LOCATOR).element

    def click_open_with(self, app_name):
        apps_list = self.__find_open_with_list()
        for app in apps_list:
            if app_name.capitalize() == app.find_element_by_id('android:id/text1').text:
                app.click()
                break
