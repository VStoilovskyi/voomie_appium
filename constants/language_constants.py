from enum import Enum


class LanguageOption(Enum):
    ENGLISH = 1
    HINDI = 2


class ErrorText(object):
    DURATION = 'Video duration must be at minimum 3 seconds and maximum 60 seconds'
