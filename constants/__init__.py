from .navigation_constants import eng_nav_text, NavigationTextEng, NavigationTextHindi
from .language_constants import LanguageOption, ErrorText
from .browsers import Browsers
from .configs import caps
