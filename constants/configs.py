import os

caps = {
    'app': os.path.abspath('../app-debug.apk'),
    'automationName': 'UIAutomator2',
    'platformName': 'Android',
    # 'platformVersion': '5.1.1',
    'deviceName': 'Android device',
    'noReset': True,
    'autoGrantPermissions': True,
    'fullReset': False,
    'appPackage': "com.voomie.videolivewallpaper",
    'appActivity': "com.voomie.videolivewallpaper.ui.screens.splash.SplashActivity",
    'uiautomator2ServerInstallTimeout': 50000,
    'newCommandTimeout': 70,
    'skipServerInstallation': True
}

server_url = 'http://127.0.0.1:4723/wd/hub'
