import os


class Video:

    folder = os.path.dirname(os.path.abspath(__file__))

    def mp4_10s(self):
        file = f"{self.folder}/source/video_10s.mp4"
        return file
