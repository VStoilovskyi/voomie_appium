import base64

with open("source/video_10s.mp4", "rb") as videoFile:
    text = base64.b64encode(videoFile.read())
    file = open("encoded/video_10s_mp4.txt", "wb")
    file.write(text)
    file.close()

    fh = open("video.mp4", "wb")
    fh.write(base64.b64decode(text))
    fh.close()
