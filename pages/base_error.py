from element import Core
from locators import ErrorLocators


class BaseError(object):
    def __init__(self, driver):
        self._element = Core(driver)

    def get_text(self):
        return self._element.find(ErrorLocators.TEXT).get_text()

    def click_ok(self):
        self._element.find(ErrorLocators.OK).click()
