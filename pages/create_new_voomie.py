from element import Core
from locators import CreateVoomieLocators


class CreateNewVoomiePage(object):
    def __init__(self, driver):
        self._driver = driver
        self._element = Core(driver)

    def click_voomie_video(self, index):
        element = self._element.find_list(CreateVoomieLocators.VOOMIE_VIDEOS_LIST).get(index).element
        element.find_element_by_id(CreateVoomieLocators.VOOMIE_VIDEO_ELEMENT.locator).click()

    def click_camera_btn(self):
        self._element.find(CreateVoomieLocators.CAMERA_LOCATOR).click()

