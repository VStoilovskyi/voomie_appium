import time
from element import Core
from locators import CameraLocators


class Camera(object):
    def __init__(self, driver):
        self._driver = driver
        self._element = Core(driver)

    def capture(self, duration):
        self._element.find(CameraLocators.CAPTURE).click()
        time.sleep(duration)
        self._element.find(CameraLocators.CAPTURE).click()
        self._element.find(CameraLocators.DONE).click()
