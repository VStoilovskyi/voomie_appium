from element import Core
from locators import HowToPageLocators


class HowToPage(object):
    def __init__(self, driver):
        self._driver = driver
        self._element = Core(driver)

    def skip_how_to_video(self):
        skip = self._element.find(HowToPageLocators.SKIP)
        skip.click() if skip.element else None
