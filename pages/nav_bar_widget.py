from element import Core
from locators import NavBarLocators as Locators, LanguageSelector
from constants import LanguageOption
from pages import HomePage
from selenium.common.exceptions import NoSuchElementException


class NavBarWidget(object):
    def __init__(self, driver):
        self._driver = driver
        self._element = Core(driver)

    def open_nav_bar_widget(self):
        HomePage(self._driver).click_nav_icon()
        return self

    def __get_nav_items(self):
        elements_list = self._element.find_list(Locators.LIST_ELEMENT).list_element
        return list(map(lambda x: x.find_element_by_xpath(".//android.widget.TextView").text, elements_list))

    def compare_navitems_text(self, nav_text_list):
        return nav_text_list == self.__get_nav_items()

    def click_change_lang_btn(self):
        self._element.find(Locators.LANGUAGE).click()
        return self

    def select_language(self, lang_option):
        lang = (LanguageSelector.HINDI, LanguageSelector.ENGLISH)[lang_option == LanguageOption.ENGLISH]
        self._element.find(lang).click()

        return self._element.find(LanguageSelector.get_lang_text_by_option_id(lang_option.value)).element

    def is_lang_selected(self, lang_option):
        check_mark = self._element.find(LanguageSelector.CHECK_ICON).element
        return check_mark.find_element_by_xpath('../android.widget.TextView')
