from element import Core
from locators import HomePageLocators as Locators
from .create_new_voomie import CreateNewVoomiePage


class HomePage(object):
    def __init__(self, driver):
        self._driver = driver
        self._element = Core(driver)

    def click_new_voomie(self):
        self._element.find(Locators.CREATE_NEW_VOOMIE_BTN).click()
        return CreateNewVoomiePage

    def click_nav_icon(self):
        try:
            self._element.find(Locators().nav_icon()).click()
        except AttributeError:
            self._element.find(Locators(is_english=False).nav_icon()).click()

    def is_loop_switcher_on(self):
        return self._element.find(Locators.LOOP_SWITCHER).get_value('checked')

    def is_active_switcher_off(self):
        status = self._element.find(Locators.WALLPAPER_SWITCHER).get_value('checked')
        return True if not status else False

