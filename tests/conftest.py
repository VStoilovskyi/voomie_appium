import pytest
from appium import webdriver
from constants.configs import caps, server_url
from pages import HowToPage


def create_driver():
    wd = webdriver.Remote(
        command_executor=server_url,
        desired_capabilities=caps
    )
    HowToPage(wd).skip_how_to_video()
    return wd


@pytest.yield_fixture(scope='module')
def driver():
    appium_driver = create_driver()
    yield appium_driver
    appium_driver.quit()


@pytest.fixture
def start(driver):
    return driver.start_activity(caps['appPackage'],
                                 caps['appActivity'])
