from pages import NavBarWidget
from constants import eng_nav_text, LanguageOption


class TestNavigationFunctions:

    def test_nav_elements_present(self, start):
        page = NavBarWidget(start)
        page.open_nav_bar_widget()

        assert page.compare_navitems_text(eng_nav_text)

    def test_if_language_selected(self, start):
        page = NavBarWidget(start)
        page.open_nav_bar_widget()
        page.click_change_lang_btn()
        assert page.select_language(LanguageOption.ENGLISH)
