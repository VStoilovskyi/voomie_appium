from services.firebase import send_push, PreparedPush
from services.notification import Notification
from services.open_with import OpenWith
from constants import Browsers


class TestNotifications:

    def test_notification_to_be_open_in_browser(self, start):
        notification = Notification(start)
        notification.open_notifications()
        send_push(**PreparedPush.TO_BE_OPEN_IN_BROWSER)
        # click on notification
        notification.find_notification_by_title_and_click(PreparedPush.TO_BE_OPEN_IN_BROWSER['title'])
        OpenWith(start).click_open_with(Browsers.CHROME)
