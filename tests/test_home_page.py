from pages import HomePage


class TestHomePage(object):

    def test_check_home_switchers(self, start):
        page = HomePage(start)
        assert page.is_loop_switcher_on()
        assert not page.is_active_switcher_off()
