import pytest
import pages
from constants import ErrorText


class TestHomePage(object):
    INVALID_DURATION = [2, 63]
    VALID_DURATION = [4, 59]

    @pytest.fixture()
    def duration_param(self, request):
        return request.param

    @pytest.mark.parametrize("duration_param", INVALID_DURATION)
    def test_invalid_camera_capture(self, start, duration_param):
        pages.HomePage(start).click_new_voomie()
        new_voomie = pages.CreateNewVoomiePage(start)
        new_voomie.click_camera_btn()
        camera = pages.Camera(start)
        camera.capture(duration_param)
        error = pages.BaseError(start)
        assert ErrorText.DURATION == error.get_text()

    @pytest.mark.parametrize("duration_param", VALID_DURATION)
    def test_valid_camera_capture(self, start, duration_param):
        pages.HomePage(start).click_new_voomie()
        new_voomie = pages.CreateNewVoomiePage(start)
        new_voomie.click_camera_btn()
        camera = pages.Camera(start)
        camera.capture(duration_param)
        error = pages.BaseError(start)
        assert not error.get_text()
